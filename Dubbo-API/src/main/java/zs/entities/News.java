package zs.entities;

import java.io.Serializable;
import java.util.Date;

public class News implements Serializable{
	private int id;
	private Integer categoryId;/* 分类ID */
	private String categoryName;/* 分类名称 */
	private String title;/* 标题 */
	private String summary;/* 摘要 */
	private String suthor;/* 作者 */
	private Date createDate;/* 创建时间 */
	private Date updateDate;/* 修改时间 */
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public String getSuthor() {
		return suthor;
	}
	public void setSuthor(String suthor) {
		this.suthor = suthor;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	@Override
	public String toString() {
		return "News [id=" + id + ", categoryId=" + categoryId + ", categoryName=" + categoryName + ", title=" + title
				+ ", summary=" + summary + ", suthor=" + suthor + ", createDate=" + createDate + ", updateDate="
				+ updateDate + "]";
	}
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	
}
