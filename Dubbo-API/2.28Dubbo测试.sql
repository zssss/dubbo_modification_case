


/* 如果存在就删除库 */
DROP Database If Exists news;
/* 如果不存在就创建库 */
Create Database If Not Exists news Character Set UTF8;
/* 使用表 */
use news;

/* 表 */
DROP TABLE IF EXISTS `news_category`;
CREATE TABLE `news_category` (
  id int(10) not null primary key auto_increment COMMENT '表的主键',
	name VARCHAR(50) COMMENT '分类名字'
      ); 
INSERT INTO `news_category` (`name`) VALUES ('新闻');
INSERT INTO `news_category` (`name`) VALUES ('娱乐');
INSERT INTO `news_category` (`name`) VALUES ('体育');

/* 表 */
DROP TABLE IF EXISTS `news_detail`;
CREATE TABLE `news_detail` (
  id int(10) not null primary key auto_increment COMMENT '表的主键',
  categoryId int COMMENT '新闻分类编号',
	title VARCHAR(50),
	summary VARCHAR(100),
	suthor VARCHAR(30),
	createDate Date COMMENT '创建时间',
	updateDate DATE COMMENT '更新时间'
      ); 
INSERT INTO `news_detail` (`categoryId`, `title`, `summary`, `suthor`, `createDate`, `updateDate`) VALUES ('1', '测试新闻1', '摘要', '小明', '2019-02-28', '2019-02-28');
INSERT INTO `news_detail` (`categoryId`, `title`, `summary`, `suthor`, `createDate`, `updateDate`) VALUES ('1', '测试新闻2', '摘要', '小明', '2019-02-28', '2019-02-28');
INSERT INTO `news_detail` (`categoryId`, `title`, `summary`, `suthor`, `createDate`, `updateDate`) VALUES ('2', '测试娱乐1', '摘要', '小明', '2019-02-28', '2019-02-28');
INSERT INTO `news_detail` (`categoryId`, `title`, `summary`, `suthor`, `createDate`, `updateDate`) VALUES ('2', '测试娱乐2', '摘要', '小明', '2019-02-28', '2019-02-28');
INSERT INTO `news_detail` (`categoryId`, `title`, `summary`, `suthor`, `createDate`, `updateDate`) VALUES ('3', '测试体育1', '摘要', '小明', '2019-02-28', '2019-02-28');
INSERT INTO `news_detail` (`categoryId`, `title`, `summary`, `suthor`, `createDate`, `updateDate`) VALUES ('3', '测试体育2', '摘要', '小明', '2019-02-28', '2019-02-28');