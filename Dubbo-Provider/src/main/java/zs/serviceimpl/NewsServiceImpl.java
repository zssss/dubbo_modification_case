package zs.serviceimpl;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.dubbo.config.annotation.Service;

import zs.API;
import zs.entities.News;
import zs.entities.NewsType;
import zs.service.NewsService;
@Service
@Component
public class NewsServiceImpl implements API{

	@Autowired
	private NewsService ns;
	
	@Override
	public News getNewsId(Integer id) {
		return ns.getNewsId(id);
	}

	@Override
	public NewsType getNewsTypeId(Integer id) {
		return ns.getNewsTypeId(id);
	}

	@Override
	public Integer UpdataNews(News news) {
		return ns.UpdataNews(news);
	}

	@Override
	public List<News> SelectNews(Integer id,String name) {
		// TODO Auto-generated method stub
		return ns.SelectNews(id,name);
	}

	@Override
	public List<NewsType> SelectNewsType() {
		// TODO Auto-generated method stub
		return ns.SelectNewsType();
	}

}
