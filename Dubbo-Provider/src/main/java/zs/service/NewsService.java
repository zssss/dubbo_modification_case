package zs.service;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import zs.entities.News;
import zs.entities.NewsType;
@Mapper
public interface NewsService {
	public List<News> SelectNews(@Param("id")Integer id,@Param("name")String name);
	public List<NewsType> SelectNewsType();
	public News getNewsId(Integer id);/* 根据Id获取News */
	public NewsType getNewsTypeId(Integer id);/* 根据Id获取NewsType */
	public Integer UpdataNews(News news);/* 修改News */

}
