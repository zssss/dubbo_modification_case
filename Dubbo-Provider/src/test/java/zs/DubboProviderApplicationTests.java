package zs;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import zs.entities.News;
import zs.entities.NewsType;
import zs.service.NewsService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DubboProviderApplicationTests {

	@Autowired
	private NewsService ns;
	
	
	@Test
	public void contextLoads() {
		NewsType nt = ns.getNewsTypeId(1);
		List<News> n = ns.SelectNews(1,null);
		List<NewsType> t = ns.SelectNewsType();
		System.out.println(nt);
		System.out.println(n);
		System.out.println(t);
	}

}
