package zs.serivceimpl;

import java.util.List;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;

import zs.API;
import zs.entities.News;
import zs.entities.NewsType;
import zs.serivce.NewsSerivceConsumer;
@Service
public class NewsServiceImplConsumer implements NewsSerivceConsumer{

	@Reference
	private API a;
	
	@Override
	public News getNewsId(Integer id) {
		return a.getNewsId(id);
	}

	@Override
	public NewsType getNewsTypeId(Integer id) {
		return a.getNewsTypeId(id);
	}

	@Override
	public Integer UpdataNews(News news) {
		return a.UpdataNews(news);
	}

	@Override
	public List<News> SelectNews(Integer id,String name) {
		// TODO Auto-generated method stub
		return a.SelectNews(id,name);
	}

	@Override
	public List<NewsType> SelectNewsType() {
		// TODO Auto-generated method stub
		return a.SelectNewsType();
	}

}
