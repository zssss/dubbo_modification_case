package zs.serivce;

import java.util.List;

import zs.entities.News;
import zs.entities.NewsType;

public interface NewsSerivceConsumer {
	public List<News> SelectNews(Integer id,String name);
	public List<NewsType> SelectNewsType();
	public News getNewsId(Integer id);/* 根据Id获取News */
	public NewsType getNewsTypeId(Integer id);/* 根据Id获取NewsType */
	public Integer UpdataNews(News news);/* 修改News */

}
