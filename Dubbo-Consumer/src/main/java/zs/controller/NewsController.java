package zs.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import zs.entities.News;
import zs.entities.NewsType;
import zs.serivce.NewsSerivceConsumer;

@Controller
public class NewsController {

	@Autowired
	private NewsSerivceConsumer nsc;

	@GetMapping("/Index")
	public String getNews(Map<String,Object> map){
		List<News> n = nsc.SelectNews(null,null);
		List<NewsType> t = nsc.SelectNewsType();
		map.put("News", n);
		map.put("NewsType", t);
		return "Index";
	}
	@GetMapping("/select")
	public String select(Integer id,String name,Map<String,Object> map){
		System.out.println(id+"---"+name);
		List<News> n = nsc.SelectNews(id,name);
		List<NewsType> t = nsc.SelectNewsType();
		map.remove("News");
		map.remove("NewsType");
		map.put("News", n);
		map.put("NewsType", t);
		System.out.println(n);
		return "Index";
	}
	@GetMapping("/getNewsId/{id}")
	public String getNewsId(@PathVariable("id")Integer id,Map<String,Object> map){
		News n = nsc.getNewsId(id);
		List<NewsType> t = nsc.SelectNewsType();
		NewsType tt = nsc.getNewsTypeId(n.getCategoryId());
		n.setCategoryName(tt.getName());
		map.put("NewsType", t);
		map.put("News", n);
		return "News";
	}
	
	@PostMapping("/Update")
	public String getNewsId(News n,Map<String,Object> map){
		System.out.println("--->"+n);
		Integer i = nsc.UpdataNews(n);
		System.out.println(i>0?"Yes":"No");
		return "redirect:/Index";
	}
	
}
